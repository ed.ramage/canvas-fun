import Entity from '/js/Entity.js';
import { loadPlayerSprite } from '/js/sprites.js';
import Velocity from '/js/traits/Velocity.js'
import Jump from '/js/traits/Jump.js'

export const createPlayer = () => {
    return loadPlayerSprite().then(sprite => {
    const player = new Entity();

    player.addTrait(new Velocity());
    player.addTrait(new Jump());

    player.draw = (ctx) => {
      sprite.draw('idle', ctx, player.pos.x, player.pos.y)  
    }

    return player;
    })
}