import { Trait } from '/js/Entity.js';

export default class Jump extends Trait {
    constructor() {
      super('jump');

      this.duration = 0.5;
      this.velocity = 200;
      this.time = 0;
    }

    start() {
        this.time = this.duration;
    }

    cancel() {
        this.time = 0;
    }

    update(entity, dt) {
        if (this.time > 0) {
            entity.vel.y = -this.velocity;
            this.time -= dt;
        }
    }
}

