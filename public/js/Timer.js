export default class Timer {
    constructor(dt = 1/60) {
      let at = 0;
      let lt = 0;

      this.updateProxy = (time) => {
            at += (time - lt) / 1000;
             while(at > dt) {
                 this.update(dt)
                 at -= dt
            }
            lt = time;
            this.enqueue();
        }
    }
    enqueue() {
        requestAnimationFrame(this.updateProxy);

    }
    start() {
         this.enqueue();
    }
}
