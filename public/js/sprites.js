import { loadImage } from '/js/loaders.js';
import SpriteSheet from '/js/SpriteSheet.js';

export const loadPlayerSprite = () => {
    return loadImage('/img/sprites.gif')
    .then(img => {
      const sprites = new SpriteSheet(img, 16, 16);
      sprites.define('idle', 276, 106, 16, 16);
      return sprites
})};

export const loadBGSprites = () => {
    return loadImage('/img/ts1.png')
  .then(img => {
    const sprites = new SpriteSheet(img, 16, 16);
    sprites.defineTile('ground', 0,2);
    sprites.defineTile('sky',3,23);

    return sprites
})};
