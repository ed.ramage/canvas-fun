import { Vec2 } from '/js/calc.js';

export class Trait {
    constructor(name) {
        this.NAME = name;
    }

    update() {
        console.warn(`Trait ${this.NAME} has no update method wtf why`);
    }
}

export default class Entity {
    constructor() {
        this.pos = new Vec2(0,0);
        this.vel = new Vec2(0,0);
        this.traits = [];
    }

    addTrait(trait) {
        this.traits.push(trait);
        this[trait.NAME] = trait;
    }

    update(dt) {
        this.traits.forEach(t => {
            t.update(this, dt)
        })
    }
}
