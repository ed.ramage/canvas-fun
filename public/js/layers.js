const drawBG = (bg, ctx, sprites) => {
    bg.ranges.forEach(([x1, x2, y1, y2])=> {
        for (let x = x1; x < x2; x++) {
            for (let y = y1; y < y2; y++) {
                sprites.drawTile(bg.tile, ctx, x, y);
            }
        }
    });
};
export const bgLayer = (bgs, sprites) => {
  const buffer = document.createElement('canvas');
  buffer.width = 256;
  buffer.height = 240;
  bgs.forEach(bg => {
       drawBG(bg, buffer.getContext('2d'), sprites)
   })

   return (ctx) => ctx.drawImage(buffer, 0, 0)
}

export const createSpriteLayer = (entity) => (ctx) => entity.draw(ctx)