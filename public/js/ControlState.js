//TODO add typescript and make these enums
const PRESS = 0;
const RELEASE = 1;

export default class ControlState {
    constructor() {
        this.keyStates = new Map();
        this.keyMap = new Map();
    }

    addMapping(keyCode, callback) {
        this.keyMap.set(keyCode, callback)
    }

    handleEvent(evt) {
        const { keyCode } = evt;
        if (!this.keyMap.has(keyCode)) {
            return;
        }
        evt.preventDefault();
        const keyState = evt.type === 'keydown' ? PRESS : RELEASE;

        if (this.keyStates.get(keyCode) === keyState) {
            return;
        }
        this.keyStates.set(keyCode, keyState);
        this.keyMap.get(keyCode)(keyState);
    }

    listenTo(window) {
        ['keydown', 'keyup'].forEach(x => {
        window.addEventListener(x, evt => {
            this.handleEvent(evt);
        })
      })
    }
}
