import { loadLevel } from '/js/loaders.js';
import { loadBGSprites } from '/js/sprites.js';
import { createPlayer } from '/js/player.js';
import Compositor from '/js/Compositor.js';
import { bgLayer, createSpriteLayer } from '/js/layers.js';
import Timer from '/js/Timer.js';
import Controls from '/js/ControlState.js';

const c = document.getElementById('canvas');
const ctx = c.getContext('2d');

const grav = 2000;

Promise.all([
    createPlayer(),
    loadBGSprites(),
    loadLevel('1-1')])
      .then(([player, bg_sprites, l]) => {
        const compo = new Compositor();
        const bgl = bgLayer(l.backgrounds, bg_sprites);
        compo.layers.push(bgl);
        player.pos.set(64,180);

        const input = new Controls();
        input.addMapping(32, keyState => {
            if (keyState)  {
              player.jump.start();
            } else {
              player.jump.cancel();
            }
        })
        input.listenTo(window);

        const sl = createSpriteLayer(player);
        compo.layers.push(sl);

        const timer = new Timer(1/60);
                                           
        timer.update = (dt) => {
            player.update(dt);
            compo.draw(ctx);
            player.vel.y += grav * dt;
        }

        timer.start();
});
