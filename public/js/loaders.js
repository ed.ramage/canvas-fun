export const loadImage = (url) => {
    return new Promise(resolve => {
        const img = new Image();
        img.addEventListener('load', () => {
            resolve(img);
        });
        img.src = url;
    })
};

export const loadLevel = (name) => {
    return fetch(`/levels/${name}.json`)
       .then(r => r.json());
};
